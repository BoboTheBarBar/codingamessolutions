package puzzles.clearLanding;

import java.util.*;

public class ClearLanding {

    public static void main(String[] args) {
        System.out.println(convertListToString2(Arrays.asList(3,5,2)));
        System.out.println(convertListToString(Arrays.asList(3,5,2)));
        Scanner in = new Scanner(System.in);
        while (true) {
            printHighestMountain(getMountains(in));
        }
    }

    private static void printHighestMountain(NavigableMap<Integer, Integer> mountains) {
        Integer index = mountains.descendingMap().firstEntry().getValue();
        System.out.println(index); // The index of the mountain to fire on.
    }

    private static NavigableMap<Integer, Integer> getMountains(Scanner in) {
        NavigableMap<Integer, Integer> mountains = createSortingMap();
        for (int i = 0; i < 8; i++) {
            mountains.put(in.nextInt(), i);
        }
        return mountains;
    }

    private static NavigableMap<Integer, Integer> createSortingMap() {
        return new TreeMap<>((o1, o2) -> o1 > o2 ? 1 : -1);
    }

    private static <T> String convertListToString(List<T> list) {
        StringBuilder b = new StringBuilder();
        list.forEach(s -> {
            b.append(s);
            if(list.lastIndexOf(s) < list.size() -1) {
                b.append(" ");
            }
        });
        return b.toString();
    }

    private static <T> String convertListToString2(List<T> list) {
        return list.stream().map(Object::toString).reduce("",
                (s1, s2) -> s1 + " " + s2
        );
    }
}
