package puzzles.shadowOfTheKnight;
import lombok.EqualsAndHashCode;

import java.util.Scanner;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;

public class Player {

    public static void main(String[] a) {
        Scanner inputs = new Scanner(System.in);
        Constraints borders = new Constraints(new Coordinates(0, 0), new Coordinates(inputs.nextInt(), inputs.nextInt()));
        inputs.nextInt();
        Coordinates batman = new Coordinates(inputs.nextInt(), inputs.nextInt());

        Player.disarmBomb(inputs, batman, borders);
    }

    public static void disarmBomb(Scanner inputs, Coordinates batman, Constraints borders) {
        while(true) {
            String bombDir = inputs.next();
            Coordinates jump = new Coordinates(0,0);
            for(char dir : bombDir.toCharArray()) {
                jump = jump.add(calcJumpDirCoord(dir, batman, borders));
            }
            borders = calcNewBorder(jump, batman, borders);
            batman = new Coordinates(batman.x + jump.x, batman.y + jump.y);
            System.out.println(batman);
        }
    }

    public static Constraints calcNewBorder(Coordinates jump, Coordinates player, Constraints borders) {
        Coordinates minConstraint = borders.minConstraint;
        Coordinates maxConstraint = borders.maxConstraint;

        BiFunction<Integer, Coordinates, Coordinates> moveX = (newX, constraint) -> new Coordinates(newX, constraint.y);
        BiFunction<Integer, Coordinates, Coordinates> moveY = (newY, constraint) -> new Coordinates(constraint.x, newY);

        if(jump.x < 0) maxConstraint = moveX.apply(player.x - 1, maxConstraint);
        else if(jump.x > 0) minConstraint = moveX.apply(player.x + 1, minConstraint);

        if(jump.y < 0) maxConstraint = moveY.apply(player.y - 1, maxConstraint);
        else if(jump.y > 0) minConstraint = moveY.apply(player.y + 1, minConstraint);

        return new Constraints(minConstraint, maxConstraint);
    }

    public static Coordinates calcJumpDirCoord(char direction, Coordinates player, Constraints constraints) {
        int xDelta = 0;
        int yDelta = 0;
        BinaryOperator<Integer> calcDif = (base, value) -> {
            double diff = ((double)base - value)/2;
            double sign = Math.signum(diff);
            return (int)(sign*Math.ceil(Math.abs(diff)));
        };
        if(direction == 'D') yDelta = calcDif.apply(constraints.maxConstraint.y,player.y);
        else if (direction == 'R') xDelta = calcDif.apply(constraints.maxConstraint.x,player.x);
        else if (direction == 'L') xDelta = calcDif.apply(constraints.minConstraint.x, player.x);
        else if (direction == 'U') yDelta = calcDif.apply(constraints.minConstraint.y,player.y);
        else throw new IllegalArgumentException("wront Input: " + direction);

        return new Coordinates(xDelta, yDelta);
    }

    @EqualsAndHashCode
    static class Coordinates {
        final int x;
        final int y;

        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }
        public String toString() {
            return x + " " + y;
        }

        public Coordinates add(Coordinates jump) {
            return new Coordinates(x + jump.x, y + jump.y);
        }
    }

    @EqualsAndHashCode
    static class Constraints {
        final Coordinates minConstraint;
        final Coordinates maxConstraint;

        public Constraints(Coordinates minConstraint, Coordinates maxConstraint) {
            this.minConstraint = minConstraint;
            this.maxConstraint = maxConstraint;
        }

        public String toString() {
            return minConstraint + " | " + maxConstraint;
        }
    }
}
