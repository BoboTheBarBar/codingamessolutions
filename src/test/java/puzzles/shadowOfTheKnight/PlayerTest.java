package puzzles.shadowOfTheKnight;

import org.junit.jupiter.api.Test;
import puzzles.shadowOfTheKnight.Player.Coordinates;
import puzzles.shadowOfTheKnight.Player.Constraints;

import static org.junit.jupiter.api.Assertions.*;

public class PlayerTest {

    private Player.Coordinates player;

    @Test
    void calcNewBorderMaxY() {
        player = new Coordinates(0, 2);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(0, 5));
        Coordinates jumpUp = new Coordinates(0, -2);

        Constraints actual = Player.calcNewBorder(jumpUp, player, constraints);
        Constraints expected = new Constraints(constraints.minConstraint, new Coordinates(0, 1));
        assertEquals(expected, actual);
    }

    @Test
    void calcNewBorderMaxX() {
        player = new Coordinates(2, 0);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(5, 0));
        Coordinates jumpLeft = new Coordinates(-2, 0);

        Constraints actual = Player.calcNewBorder(jumpLeft, player, constraints);
        Constraints expected = new Constraints(constraints.minConstraint, new Coordinates(1, 0));
        assertEquals(expected, actual);
    }

    @Test
    void calcNewBorderMinX() {
        player = new Coordinates(2, 0);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(5, 5));
        Coordinates jumpRight = new Coordinates(2, 0);

        Constraints actual = Player.calcNewBorder(jumpRight, player, constraints);
        Constraints expected = new Constraints(new Coordinates(3, 0), constraints.maxConstraint);
        assertEquals(expected, actual);
    }

    @Test
    void calcNewBorderMinY() {
        player = new Coordinates(0, 2);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(5, 5));
        Coordinates jumpDown = new Coordinates(0, 2);

        Constraints actual = Player.calcNewBorder(jumpDown, player, constraints);
        Constraints expected = new Constraints(new Coordinates(0, 3), constraints.maxConstraint);
        assertEquals(expected, actual);
    }

    @Test
    void calcNewBorderCombo() {
        player = new Coordinates(2, 2);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(5, 5));
        Coordinates jumpCombo = new Coordinates(-1, 2);

        Constraints actual = Player.calcNewBorder(jumpCombo, player, constraints);
        Constraints expected = new Constraints(new Coordinates(0, 3), new Coordinates(1, 5));
        assertEquals(expected, actual);
    }

    @Test
    void calcJumpDirCoordU() {
        player = new Coordinates(3, 3);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(6, 6));
        Coordinates actualJump = Player.calcJumpDirCoord('U', player, constraints);
        Coordinates expectedJump = new Coordinates(0, -2);
        assertEquals(expectedJump.x, actualJump.x);
        assertEquals(expectedJump.y, actualJump.y);
    }

    @Test
    void calcJumpDirCoordD() {
        player = new Coordinates(3, 3);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(6, 6));
        Coordinates actualJump = Player.calcJumpDirCoord('D', player, constraints);
        Coordinates expectedJump = new Coordinates(0, 2);
        assertEquals(expectedJump.x, actualJump.x);
        assertEquals(expectedJump.y, actualJump.y);
    }

    @Test
    void calcJumpDirCoordL() {
        player = new Coordinates(3, 3);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(6, 6));
        Coordinates actualJump = Player.calcJumpDirCoord('L', player, constraints);
        Coordinates expectedJump = new Coordinates(-2, 0);
        assertEquals(expectedJump.x, actualJump.x);
        assertEquals(expectedJump.y, actualJump.y);
    }

    @Test
    void calcJumpDirCoordR() {
        player = new Coordinates(3, 3);
        Constraints constraints = new Constraints(new Coordinates(0, 0), new Coordinates(6, 6));
        Coordinates actualJump = Player.calcJumpDirCoord('R', player, constraints);
        Coordinates expectedJump = new Coordinates(2, 0);
        assertEquals(expectedJump.x, actualJump.x);
        assertEquals(expectedJump.y, actualJump.y);
    }
}